# CoreMedia - content-management-server

## Ports

```
40109
40180
40198
40199
37705
40105
```

## dependent services

### solr
```
content_management:
  solr:
    url: http://127.0.0.1:40080/solr
```

### master-live-server
```
content_management:
  publisher:
    target:
      ior_url: http://mls.cm.local:40280/master-live-server/ior
```

### database
```
content_management_database:
  host: backend_database.int
  port: 3306
  schema: cm_management
  user: cm_management
  password: cm_management
```


![setup](content-management-server.png)
